<?php

namespace  KDA\Filament\TranslationManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Eloquent\I18nCollector\Models\Key as ModelsKey;
use KDA\Eloquent\MedialibraryItem\Models\Traits\CuratesMedia;

class Key extends ModelsKey
{
   
    use CuratesMedia;

 
}

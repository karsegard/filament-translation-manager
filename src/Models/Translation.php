<?php

namespace  KDA\Filament\TranslationManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Eloquent\I18nCollector\Models\Translation as ModelsTranslation;
use KDA\Eloquent\MedialibraryItem\Models\Traits\CuratesMedia;

class Translation extends ModelsTranslation
{
   



    public function key(){
        return $this->belongsTo(Key::class,'key_id');
    }

 
}

<?php

namespace KDA\Filament\TranslationManager;
use Filament\PluginServiceProvider;
use Spatie\LaravelPackageTools\Package;
use KDA\Filament\TranslationManager\Filament\Resources;
use KDA\Filament\TranslationManager\Filament\Resources\TranslationResource;

class FilamentServiceProvider extends PluginServiceProvider
{
    //protected array $styles = [
    //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    //];

    protected array $widgets = [
    //    CustomWidget::class,
        TranslationResource\Widgets\TranslationsOverview::class
    ];

   // protected array $pages = [
    //    CustomPage::class,
    //];

    protected array $resources = [
   //     CustomResource::class,
        Resources\TranslationResource::class,
        Resources\ApplicationResource::class,
        //Resources\CategoryResource::class,
        Resources\ContributorLanguageResource::class,
        Resources\KeyResource::class,
        Resources\TranslationResource::class,
        Resources\LanguageResource::class,
        Resources\RequestResource::class,
    ];
 
    public function configurePackage(Package $package): void
    {
        $package->name('filament-translation-manager');
    }
}

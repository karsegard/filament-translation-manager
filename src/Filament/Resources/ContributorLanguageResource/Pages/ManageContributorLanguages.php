<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\ContributorLanguageResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\ContributorLanguageResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ManageRecords;

class ManageContributorLanguages extends ManageRecords
{
    protected static string $resource = ContributorLanguageResource::class;

    protected function getActions(): array
    {
        return [
        ];
    }
}

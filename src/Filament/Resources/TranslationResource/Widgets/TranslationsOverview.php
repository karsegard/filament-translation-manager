<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\TranslationResource\Widgets;

use Filament\Pages\Page;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;
use Illuminate\Support\Facades\Route;
use KDA\Eloquent\I18nCollector\Models\Translation;

class TranslationsOverview extends BaseWidget
{
    public static function canView(): bool
    {
        if(!auth()->user()->contributableCategories?->count() || !auth()->user()->contributableLanguages()?->count()){
            return false;
        }
        if( config('kda.filament-translation-manager.hide_widget_on_dashboard') && Route::is('filament.pages.dashboard')){
            return false;
        }
        return true;
    }
    protected function getCards(): array
    {
        // dump(request()->query('tableFilters'));

        $tr = Translation::forCollaborator(auth()->user());
        $total = $tr->count();
        $remaining =  $tr->needingTranslation()->count();

        $pct_done = $total > 0 ? ($total - $remaining) * 100 / $total : 0;
        return [
            Card::make('Remaning translations', $remaining),
            Card::make('Completion', floor($pct_done) . '%'),
        ];
    }
}

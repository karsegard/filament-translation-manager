<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\TranslationResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\TranslationResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTranslation extends CreateRecord
{
    protected static string $resource = TranslationResource::class;
}

<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\TranslationResource\Pages;

use Filament\Notifications\Notification;
use KDA\Filament\TranslationManager\Filament\Resources\TranslationResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use Filament\Pages\Actions\Action;
use KDA\Eloquent\I18nCollector\Models\Request;

class EditTranslation extends EditRecord
{
    protected static string $resource = TranslationResource::class;
    protected $listeners = ['refreshComponent' => '$refresh'];
    protected function getActions(): array
    {
        return [
            //1     Actions\DeleteAction::make(),
            Actions\Action::make('request')
                ->label('Request screenshot')
                ->action(function ($livewire) {
                    $record = $livewire->getRecord();
                    Request::create([
                        'language_id' => $record->language->id,
                        'key_id' => $record->key->id,
                        'screenshot' => true,
                        'requester_id' => auth()->user()->id
                    ]);
                })
                ->disabled(function ($livewire) {
                    $record = $livewire->getRecord();
                    $test =  Request::where('language_id', $record->language->id)->where('key_id', $record->key->id)->where('screenshot', true)->where('requester_id', auth()->user()->id)->first();
                    return !blank($test);
                })
                ->after(function ($livewire) {
                    Notification::make()
                        ->title('screenshot requested')
                        ->success()
                        ->send();
                    $livewire->emit('refreshComponent');
                }),
            Actions\Action::make('request_comment')
                ->label('Request comment')
                ->disabled(function ($livewire) {
                    $record = $livewire->getRecord();
                    $test =  Request::where('language_id', $record->language->id)->where('key_id', $record->key->id)->where('screenshot', false)->where('requester_id', auth()->user()->id)->first();
                    return !blank($test);
                })
                ->action(function ($livewire) {
                    $record = $livewire->getRecord();
                    Request::create([
                        'language_id' => $record->language->id,
                        'key_id' => $record->key->id,
                        'screenshot' => false,
                        'requester_id' => auth()->user()->id
                    ]);
                })->after(function ($livewire) {
                    Notification::make()
                        ->title('comment requested')
                        ->success()
                        ->send();
                    $livewire->emit('refreshComponent');
                }),
            Actions\Action::make('help')->label('Help')->url('/docs/howto_translate.pdf', true),
        ];
    }
    protected function getRedirectUrl(): string
    {
        //return $this->getResource()::getUrl('index');
        $record = $this->getRecord();

        $model = $this->getResource()::getModel();
        $r = $model::where('language_id', $record->language_id)->needingTranslation()->first();

        if ($r) {
            return $this->getResource()::getUrl('edit', ['record' => $r]);
        }
        return $this->getResource()::getUrl('index');
    }

    protected function getSaveFormAction(): Action
    {
        return Action::make('save')
            ->label("Save and edit next")
            ->submit('save')
            ->keyBindings(['mod+s']);
    }
}

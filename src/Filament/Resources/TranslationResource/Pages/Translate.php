<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\TranslationResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\TranslationResource;
use Filament\Resources\Pages\Page;

class Translate extends Page
{
    protected static string $resource = TranslationResource::class;

    protected static string $view = 'filament.resources.translation-resource.pages.translate';
}

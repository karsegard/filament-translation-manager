<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\TranslationResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\TranslationResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;
use Illuminate\Database\Eloquent\Builder;

class ListTranslations extends ListRecords
{
    protected static string $resource = TranslationResource::class;

    protected function getTableQuery(): Builder
    {
//        dump(auth()->user()->id);

        return parent::getTableQuery()->withoutGlobalScopes()->forCollaborator(auth()->user());
    }
    protected function shouldPersistTableFiltersInSession(): bool
{
    return true;
}
    protected function getActions(): array
    {
        return [
           // Actions\CreateAction::make(),
        ];
    }
    protected function getHeaderWidgets():array{
        return [
            TranslationResource\Widgets\TranslationsOverview::class,
        ];
    }
}

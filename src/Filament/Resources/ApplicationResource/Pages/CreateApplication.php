<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\ApplicationResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\ApplicationResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateApplication extends CreateRecord
{
    protected static string $resource = ApplicationResource::class;
}

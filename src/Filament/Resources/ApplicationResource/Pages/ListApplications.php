<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\ApplicationResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\ApplicationResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListApplications extends ListRecords
{
    protected static string $resource = ApplicationResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}

<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\ApplicationResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\ApplicationResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditApplication extends EditRecord
{
    protected static string $resource = ApplicationResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}

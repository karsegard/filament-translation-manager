<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\ApplicationResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\ApplicationResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ManageRecords;

class ManageApplications extends ManageRecords
{
    protected static string $resource = ApplicationResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}

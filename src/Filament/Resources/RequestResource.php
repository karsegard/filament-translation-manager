<?php
namespace KDA\Filament\TranslationManager\Filament\Resources;

use KDA\Filament\TranslationManager\Filament\Resources\RequestResource\Pages;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Actions\Action;
use Filament\Tables\Columns\BooleanColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use KDA\Eloquent\I18nCollector\Models\Request;

class RequestResource extends Resource
{
    protected static ?string $model = Request::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';
    protected static ?string $navigationGroup = 'Translation Manager';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
               
            
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
                TextColumn::make('requester.name'),
                BooleanColumn::make('screenshot'),
                Tables\Columns\TextColumn::make('key.category')->formatStateUsing(
                    function ($state) {
                        return $state->app->name . "." . $state->name;
                    }
                )->label('App'),
                TextColumn::make('key.name'),
            ])
            ->filters([
                //
                Filter::make('pending')->toggle(true)->default(true)->query(fn (Builder $query): Builder => $query->where('fullfilled', false))
            ])
            ->actions([
                Tables\Actions\Action::make('edit key')->url(fn($record)=> route('filament.resources.keys.edit',['record'=>$record->key]),true), 
                Tables\Actions\Action::make('completed')->action(fn($record)=> $record->update(['fullfilled'=>true,'author_id'=>auth()->user()->id]))->requiresConfirmation(true) 
            ])
            ->bulkActions([
            ]);
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageRequests::route('/'),
        ];
    }    
}

<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\CategoryResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\CategoryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateCategory extends CreateRecord
{
    protected static string $resource = CategoryResource::class;
}

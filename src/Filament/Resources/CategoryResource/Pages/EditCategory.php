<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\CategoryResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\CategoryResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditCategory extends EditRecord
{
    protected static string $resource = CategoryResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}

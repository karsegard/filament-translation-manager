<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\KeyResource\Pages;

use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use KDA\Filament\TranslationManager\Filament\Resources\KeyResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;
use Filament\Forms\Components\Textarea;
use KDA\Eloquent\I18nCollector\Models\Category;
use KDA\Eloquent\I18nCollector\Models\Key;

class ListKeys extends ListRecords
{
    protected static string $resource = KeyResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Actions\Action::make('import')->form([
                Select::make('category_id')
                    ->options(Category::all()->pluck('full_name', 'id'))->required(),
                Textarea::make('keys')->required()
            ])
            ->action(function($data){
                $lines = explode("\n",$data['keys']);
                foreach($lines as $line){
                    $line = trim($line);
                    Key::create([
                        'category_id'=>$data['category_id'],
                        'name'=>$line,
                    ]);
                }
            }),
        ];
    }
}

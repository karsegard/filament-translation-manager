<?php

namespace KDA\Filament\TranslationManager\Filament\Resources;

use KDA\Filament\TranslationManager\Filament\Resources\CategoryResource\Pages;
use KDA\Filament\TranslationManager\Filament\Resources\CategoryResource\RelationManagers;
use KDA\Filament\TranslationManager\Models\Category;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class CategoryResource extends Resource
{
    protected static ?string $model = \KDA\Eloquent\I18nCollector\Models\Category::class;
    protected static ?string $navigationGroup = 'Translation Manager';
    protected static ?string $navigationIcon = 'heroicon-o-translate';

    public static function form(Form $form): Form
    {
        
        return $form
            ->schema([
               
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('app.name')
                    ->url(fn ( $record): string => route('filament.resources.applications.edit', ['record' => $record])),
                Tables\Columns\TextColumn::make('name'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCategories::route('/'),
            'create' => Pages\CreateCategory::route('/create'),
            'edit' => Pages\EditCategory::route('/{record}/edit'),
        ];
    }    
}

<?php

namespace KDA\Filament\TranslationManager\Filament\Resources;

use KDA\Filament\TranslationManager\Filament\Resources\ContributorLanguageResource\Pages;
use KDA\Filament\TranslationManager\Filament\Resources\ContributorLanguageResource\RelationManagers;
use App\Models\User;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class ContributorLanguageResource extends Resource
{
    protected static ?string $model = User::class;

    protected static ?string $recordRouteKeyName = "collaborators";
    protected static ?string $slug = "collaborators";

    protected static ?string $modelLabel = "Contributeurs";
    protected static ?string $navigationGroup = 'Translation Manager';
    protected static ?string $navigationIcon = 'heroicon-o-users';
    
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                
                Forms\Components\MultiSelect::make('contributableLanguages')
                    ->required()
                    ->relationship('contributableLanguages',"name"),
                Forms\Components\MultiSelect::make('contributableCategories')
                    ->required()
                    ->relationship('contributableCategories',"name")
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
                TextColumn::make('email'),
                TextColumn::make('contributable_languages_count')->counts('contributableLanguages')->label('Assigned languages'),
                TextColumn::make('contributable_categories_count')->counts('contributableCategories')->label('Assigned categories')
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
            ]);
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ManageContributorLanguages::route('/'),
        ];
    }    
}

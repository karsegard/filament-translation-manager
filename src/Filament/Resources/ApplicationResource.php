<?php

namespace KDA\Filament\TranslationManager\Filament\Resources;

use KDA\Filament\TranslationManager\Filament\Resources\ApplicationResource\Pages;
use KDA\Filament\TranslationManager\Filament\Resources\ApplicationResource\RelationManagers;
use KDA\Filament\TranslationManager\Models\Application;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class ApplicationResource extends Resource
{
    protected static ?string $model =  \KDA\Eloquent\I18nCollector\Models\Application::class;
    protected static ?string $navigationGroup = 'Translation Manager';
    protected static ?string $navigationIcon = 'heroicon-o-desktop-computer';
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')
                ->required()
                ->maxLength(255),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name'),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
            ApplicationResource\RelationManagers\CategoriesRelationManager::class
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListApplications::route('/'),
            'create' => Pages\CreateApplication::route('/create'),
            'edit' => Pages\EditApplication::route('/{record}/edit'),
        ];
    }    
}

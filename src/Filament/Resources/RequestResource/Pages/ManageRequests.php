<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\RequestResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\RequestResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ManageRecords;
use Illuminate\Database\Eloquent\Builder;


class ManageRequests extends ManageRecords
{
    protected static string $resource = RequestResource::class;

    protected function getActions(): array
    {
        return [
        ];
    }

    protected function getTableQuery(): Builder
    {
//        dump(auth()->user()->id);

        return parent::getTableQuery()->withoutGlobalScopes();
    }
}

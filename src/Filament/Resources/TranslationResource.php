<?php

namespace KDA\Filament\TranslationManager\Filament\Resources;

use KDA\Filament\TranslationManager\Filament\Resources\TranslationResource\Pages;
use KDA\Filament\TranslationManager\Filament\Resources\TranslationResource\RelationManagers;
use Filament\Forms;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Tables\Filters\Layout;
use Illuminate\Support\HtmlString;
use KDA\Filament\TranslationManager\Models\Translation;

class TranslationResource extends Resource
{
    protected static ?string $model = Translation::class;
    protected static ?string $navigationGroup = 'Translation Manager';
    protected static ?string $navigationIcon = 'heroicon-o-translate';

    protected function shouldPersistTableFiltersInSession(): bool
{
    return true;
}

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Card::make([

                    Placeholder::make('language')
                        ->label('Translating to ')
                        ->content(fn ($record) => $record->language->name),

                    Placeholder::make('comment')
                        ->label('Developer comment')
                        ->content(fn ($record) => $record->key->comment),

                    TextInput::make('value')
                        ->required()
                        ->label(fn ($record) =>  $record->key->name_without_comment)
                        ->maxLength(255)->columnSpan('full')->autofocus(),
                    
                    Placeholder::make('translations')
                        ->label('Other translations:')
                        ->content(function ($record) {
                           return new HtmlString($record->key->translations->reduce(function($carry,$trans) use($record){
                                if($trans->id != $record->id){
                                    $result = ['lang'=>$trans->language->name, 'value'=>$trans->value];
                                    $carry->push($result);
                                }
                                return $carry;
                           },collect([]))->map(function($item){
                                return "<b>".$item['lang']."</b>: ".(!empty($item['value']) ? $item['value']: '<i>not translated yet</i>');
                           })->join("<br/>"));
                        })->columnSpan('full'),
                    
                        Placeholder::make('key.screenshot')
                        ->label('Screenshot:')
                        ->hidden(function($record){
                           return  $record->key->getMediaItemsByGroup('screenshot')
                            ?->first()?->getFirstMediaByFlavor()?->getUrl('thumb-xl') =="";
                        })
                        ->content(function ($record) {
                            $image = $record->key->getMediaItemsByGroup('screenshot')
                            ?->first()?->getFirstMediaByFlavor()?->getUrl('thumb-xl') ??"";
                           return new HtmlString("<img src=\"".$image."\">");
                        })->columnSpan('full')

                ])->columns(2)

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
                //  Tables\Columns\TextColumn::make('key.category.app.name'),
                TextColumn::make('id')->toggleable(true)->toggledHiddenByDefault(true),
                Tables\Columns\TextColumn::make('key.category')->formatStateUsing(
                    function ($state) {
                        return $state->app->name . "." . $state->name;
                    }
                )->label('App')->toggleable(isToggledHiddenByDefault: false),

                Tables\Columns\TextColumn::make('key.versions')->formatStateUsing(
                    function ($state) {
                        return collect($state)->map(function ($item) {
                            return $item->version;
                        })->join(',');
                    }
                )->label('versions')->toggleable(isToggledHiddenByDefault: false),
                Tables\Columns\TextColumn::make('key.name_without_comment')
                ->searchable(['name'])
                    ->tooltip(function ($column): ?string {
                        return  $column->getRecord()->key->comment;
                    }),

                Tables\Columns\TextColumn::make('language.name')->toggleable(isToggledHiddenByDefault: false),
                Tables\Columns\TextColumn::make('value')
                ->searchable(isIndividual: true,query:function(Builder $query, string $search){
                    return $query->whereRaw("UPPER(value) LIKE '%". strtoupper($search)."%'"); 
                }),
            ])
            ->filters([
                //
                SelectFilter::make('language_id')->label('Language')
                    ->options(auth()->user()->contributableLanguages->pluck('name', 'id')),
                    Filter::make('hide_pending_request')
                    ->toggle()
                    ->default()
                    ->query(fn (Builder $query): Builder => $query->withNoPendingRequests()),

                Filter::make('needing_translation')
                    ->toggle()
                    ->default()
                    ->query(fn (Builder $query): Builder => $query->whereNull('value'))
            ], layout: Layout::AboveContent)
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                //    Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTranslations::route('/'),
            'create' => Pages\CreateTranslation::route('/create'),
            'edit' => Pages\EditTranslation::route('/{record}/edit'),

        ];
    }
}

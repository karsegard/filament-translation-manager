<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\LanguageResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\LanguageResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditLanguage extends EditRecord
{
    protected static string $resource = LanguageResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}

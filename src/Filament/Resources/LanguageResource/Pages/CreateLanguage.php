<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\LanguageResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\LanguageResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateLanguage extends CreateRecord
{
    protected static string $resource = LanguageResource::class;
}

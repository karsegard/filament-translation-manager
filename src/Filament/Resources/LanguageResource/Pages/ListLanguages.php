<?php

namespace KDA\Filament\TranslationManager\Filament\Resources\LanguageResource\Pages;

use KDA\Filament\TranslationManager\Filament\Resources\LanguageResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListLanguages extends ListRecords
{
    protected static string $resource = LanguageResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}

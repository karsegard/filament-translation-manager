<?php

namespace KDA\Filament\TranslationManager\Filament\Resources;

use KDA\Filament\TranslationManager\Filament\Resources\KeyResource\Pages;
use KDA\Filament\TranslationManager\Filament\Resources\KeyResource\RelationManagers;
use KDA\Filament\TranslationManager\Models\Key;
use Filament\Forms;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Actions\Action;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Filters\Layout;
use KDA\Eloquent\I18nCollector\Models\Category;
use KDA\Filament\MediaManager\Forms\Components\MediaItemPicker;

class KeyResource extends Resource
{
    protected static ?string $model = Key::class;
    protected static ?string $navigationGroup = 'Translation Manager';
    protected static ?string $navigationIcon = 'heroicon-o-variable';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
                Forms\Components\TextInput::make('name')
                ->required()
                ->maxLength(255),
                TextInput::make('comment'),
                MediaItemPicker::make('screenshot')->group('screenshot')
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('id')->sortable(),
                Tables\Columns\TextColumn::make('name')->searchable(),
            ])
            ->filters([
                //
                SelectFilter::make('category_id')
                    ->options(Category::all()->pluck('full_name', 'id')),
            ],layout: Layout::AboveContent)
           
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListKeys::route('/'),
            'create' => Pages\CreateKey::route('/create'),
            'edit' => Pages\EditKey::route('/{record}/edit'),
        ];
    }    
}
